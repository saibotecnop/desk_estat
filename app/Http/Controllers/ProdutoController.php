<?php

namespace App\Http\Controllers;

use App\Produto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function produtos()
    {
        $produtos = Produto::all();
        return view('produtos.index', compact('produtos'));
    }

    public function detalhes($produtoId)
    {
    	$produtoBase = Produto::find($produtoId);
    	$produtos = Produto::where('url',$produtoBase->url)->orderBy("criado_em")->get();
    	
        return view('produtos.observacoes', compact('produtos', 'produtoBase'));	
    }

    public function edit($id){
    	$produto = Produto::find($id);
		return view('produtos.edit', compact('produto')); 
    }

    public function update(Request $request){

    	$produtos = Produto::where('url', $request->url)->get();
 
    	if ($request->has('url_foto')) {
    		if ($produtos->first()->url_foto && Storage::disk('public')->exists($produtos->first()->url_foto)) {
				Storage::disk('public')->delete($produtos->first()->url_foto);
			}
			$url_foto = Storage::disk('public')->putFile('produtos/', $request->file('url_foto'));
			Produto::where('url', $request->url)->update([
	    		'categoria'=>$request->categoria,
	    		'url_foto'=> $url_foto,
	    	]);
    	}else{
    		if($request->has('categoria')){
				Produto::where('url', $request->url)->update([
		    		'categoria'=>$request->categoria
		    	]);
    		}else{
    			$url_foto = Storage::disk('public')->putFile('produtos/', $request->file('url_foto'));
				Produto::where('url', $request->url)->update([
		    		'url_foto'=> $url_foto,
		    	]);
    		}
    	}

    	return redirect()->route('produtos')->with([
        	'status' => 'success',
        	'message' => 'Produtos editados!'
        ]);
    }

    public function estatisticas()
    {
    	$produtos = Produto::all()->groupBy(function($date) {
	        return Carbon::parse($date->criado_em)->format('d');
	    });

		return view('estatisticas', compact('produtos'));	
    }

    public function dataProduto(Request $request)
    {
    	$produtos = Produto::where('url',$request->url)->get()->sortBy('criado_em');
    	$labels = [];
    	$precos = [];
    	foreach ($produtos as $produto) {
    		array_push($labels, $produto->criado_em->format('d-M'));
    		array_push($precos, $produto->preco);
    	}
    	$bgs =[
    		'#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
			'#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
			'#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
			'#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
			'#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
			'#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
			'#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
			'#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
			'#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
			'#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
		];
    	$bg = array_rand($bgs);
    	$data = [
    		'background' => $bgs[$bg],
    		'produto' => $produtos->first(),
    		'labels' => $labels,
    		'precos' => $precos
    	];
    	return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public function donutCategorias()
    {
      	$produtos = Produto::all()->groupBy('categoria');
      	
    	$labels = [];
    	$categorias = [];
    	
    	foreach ($produtos as $key => $produtosCat) {
    		array_push($labels, str_replace('_', ' ', ucfirst($key)));
    		array_push($categorias, $produtosCat->count());
    	}
    	$data = [
    		'labels' => $labels,
    		'categorias' => $categorias
    	];
    	return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public function lineVarCategorias()
    {
    	$produtos = Produto::all();

    	$labels = [];

    	$teste = $produtos->sortBy('criado_em')->groupBy(function($date) {
	        return Carbon::parse($date->criado_em)->format('d-M');
	    });
    	foreach ($teste as $data => $produtos) {
    		array_push($labels, $data);
    	}

		$datasets = [];
		$colors = ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","##a83232","#a0a832"];
		$colorId = 0;
    	foreach (Produto::all()->sortBy('created_at')->groupBy('categoria') as $categoria => $produtosPorCat) {

    		$produtosNovo = $produtosPorCat->sortBy('criado_em')->groupBy(function($date) {
		        return Carbon::parse($date->criado_em)->format('d-M');
		    });

    		$valores = [];
		    foreach ($produtosNovo as $data => $produtosDaData) {
		    	array_push($valores, $produtosDaData->pluck('preco')->sum());
		    }

		    $dataset['data'] = $valores;
			$dataset['label'] = str_replace('_', ' ', ucfirst($categoria));
			$dataset['borderColor'] = $colors[$colorId];
			$dataset['fill'] = false;
			array_push($datasets, $dataset);
    		$colorId++;
    	}

    	$data = [
    		'labels' => $labels,
    		'datasets' => $datasets
    	];
    	return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
