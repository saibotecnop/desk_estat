<?php

namespace App\Http\Controllers;

use App\Texto;
use Illuminate\Http\Request;

class TextoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$textos = Texto::all();
        return view('sobre',compact('textos'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Texto  $texto
     * @return \Illuminate\Http\Response
     */
    public function edit(Texto $texto)
    {
        return view('textos.edit', compact('texto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Texto  $texto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Texto $texto)
    {
        $texto->titulo = $request->titulo;
        $texto->corpo = $request->corpo;
        $texto->save();

        return redirect()->route('sobre')->with([
        	'status' => 'success',
        	'message' => 'Atualizado com sucesso'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Texto  $texto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Texto $texto)
    {
        //
    }
}
