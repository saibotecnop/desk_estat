<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Produto extends Model
{
    protected $table = 'produtos';
    protected $dates = ['criado_em'];

    public function getFullUrlFotoAttribute($value)
    {
    	return $this->url_foto ? Storage::disk('public')->url($this->url_foto) : null;
    }
}
