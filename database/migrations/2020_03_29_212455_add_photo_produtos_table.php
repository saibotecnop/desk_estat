<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhotoProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
   		Schema::table('produtos', function (Blueprint $table) {
   			$table->text('categoria')->nullable()->after('preco');
        	$table->text('url_foto')->nullable()->after('categoria');
        	$table->timestamp('updated_at')->nullable()->after('criado_em');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produtos', function (Blueprint $table) {
        	$table->dropColumn('categoria');
        	$table->dropColumn('url_foto');
        	$table->dropColumn('updated_at');
        });
    }
}
