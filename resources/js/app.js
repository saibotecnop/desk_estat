/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
var Chart = require('chart.js');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

var ClassicEditor = require('@ckeditor/ckeditor5-build-classic');
jQuery(document).ready(function($) {
	image_preview();

	ClassicEditor
        .create( document.querySelector( '#texto1' ), {
        	toolbar: ['Heading', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ]
        } )
        .catch( error => {
            console.error( error );
        } );

    ClassicEditor
        .create( document.querySelector( '#texto2' ), {
        	toolbar: ['Heading', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ]
        } )
        .catch( error => {
            console.error( error );
        } );

    ClassicEditor
        .create( document.querySelector( '#texto3' ), {
        	toolbar: ['Heading', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote' ]
        } )
        .catch( error => {
            console.error( error );
        } );



	// callPreloader();
	$('.preloader').click(function(){
		callPreloader();
	});

	if ($("#singleProduct").index() != -1) {
		let url = $('#urlProduto').val();
		axios
		.post('/produtos/getData',{
			url: url
		})
		.then(function(response){
			console.log(response.data)
			new Chart(document.getElementById("singleProduct"), {
				type: 'line',
				data: {
					labels: response.data.labels,
					datasets: [{ 
						data: response.data.precos,
						label: response.data.produto.nome,
						borderColor: response.data.background,
						fill: false
					}]
			  	},
			  	options: {
			  		responsive: false,
					title: {
					display: false,
					text: 'World population per region (in millions)'
					}
			  	}
			});
		})
	}
	if ($("#donutCategorias").index() != -1) {
		callPreloader('<p>Carregando...</p>');
	}
	setTimeout(function(){
		if ($("#donutCategorias").index() != -1) {
			axios
			.post('/produtos/donutCategorias')
			.then(function(response){
				console.log(response.data)
				new Chart(document.getElementById("donutCategorias"), {
					type: 'doughnut',
					data: {
						labels: response.data.labels,
						datasets: [{ 
							label: "Population (millions)",
					          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","##a83232","#a0a832"],
					          data: response.data.categorias
						}]
				  	},
				  	options: {
				  		responsive: false,
						title: {
							display: false,
							text: 'Observações por categoria de produto'
						}
				  	}
				});
			})
		}

		if ($("#lineVarCategorias").index() != -1) {
			axios
			.post('/produtos/lineVarCategorias')
			.then(function(response){
				console.log(response.data)
				new Chart(document.getElementById("lineVarCategorias"), {
					type: 'line',
					data: {
						labels: response.data.labels,
						datasets: response.data.datasets
				  	},
				  	options: {
				  		responsive: false,
						title: {
							display: false,
							text: 'Observações por categoria de produto'
						}
				  	}
				});
			})
		}
		removePreloader(1000);
	}, 1000)
	
});

function callPreloader(message){

    let fadedMask = "<div id='fadedMask'></div>"
    let loader = '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div><div class="bounce2"></div><div class="bounce1"></div></div>';

    if ($('#fadedMask').index() == -1) {
        $('body').prepend(fadedMask);
    };

    if (message != undefined && message != "") {
        let messenger = "<div id='fadedMaskMessage'>"+message+"</div>";
        let container = "<div id='fadedMaskContainer'>"+loader+messenger+"</div>";
        $("#fadedMask").html(container);
    }else{
        $('#fadedMask').html("<div id='fadedMaskContainer'>"+loader+"</div>");
    }

    $('#fadedMask').fadeIn(300);
}

function changePreloaderMessage(message){
  	$("#fadedMask #fadedMaskMessage").html(message);
};

function removePreloader(timeout){
	let time = (timeout != undefined && timeout != '') ? timeout : 300;
    setTimeout(function(){
        $('#fadedMask').fadeOut(function(){
            $("#fadedMask").remove();

        });
    }, time);
};

function image_preview(){

	$('.upload-input').change(function(e) {
		let inputFile = $(this);
        let target = inputFile.parents('.row').find('.img-thumbnail');
        let inputButton = inputFile.next('.upload-button');
        let file = e.target.files;
        for (let i = 0, f; f = file[i]; i++) {
		    let reader = new FileReader();
		    reader.onload = (function(theFile) {
	      		return function(e) {
	      			target.prop('src', e.target.result);
	      			inputButton.html('Trocar foto')
	      		};
	  		})(f);

	      	// Read in the image file as a data URL.
			reader.readAsDataURL(f);
			if (inputFile.is(':visible')) {
				targetHolder.show();
				inputFile.hide();
			}
	  	}
	});
}
