@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
    	@include('partials.sidebar')
    	<div class="col-md-9">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="card">
		                <div class="card-header">Geral</div>

		                <div class="card-body">
							<div class="row">
								<div class="col-12 col-md-6">
									<div class="card">
										<div class="card-body">
											<h5 class="card-title">Produtos observados:</h5>
											<h1 class="card-text">{{ $produtos->groupBy('url')->count() }}</h1>
											@if ($produtos->count() > 0)
												<a href="{{ route('produtos') }}" class="btn btn-primary btn-sm">Ver todos</a>
											@endif
										</div>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="card">
										<div class="card-body">
											<h5 class="card-title">Observações:</h5>
											<h1 class="card-text">{{ $produtos->count() }}</h1>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row mt-3">
        		<div class="col-md-12">
        			<div class="card">
		                <div class="card-header">Gráficos</div>

		                <div class="card-body">
							<div class="row">
								<div class="col-12 col-md-6">
									<div class="card">
										<div class="card-header text-center">
											<strong class="card-title">Observações por categoria:</strong>
										</div>
										<div class="card-body">
											<canvas id="donutCategorias" class="d-block w-100" height=""></canvas>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="card">
										<div class="card-header text-center">
											<strong class="card-title">Variação acumulada por categoria:</strong>
										</div>
										<div class="card-body p-0">
											<ul class="list-group-flush p-0 m-0">
												@foreach ($produtos->sortBy('criado_em')->groupBy('categoria') as $categoria => $produtosPorCat) 
													<li class="list-group-item justify-content-center d-flex">
														{{ str_replace('_', ' ', ucfirst($categoria)) }} 
														@php
															$produtosPorDataECat = $produtosPorCat->sortBy('criado_em')->groupBy(function($date) {
														        return Carbon\Carbon::parse($date->criado_em)->format('d-M');
														    });

															$first = $produtosPorDataECat->first()->pluck('preco')->sum();
															$last = $produtosPorDataECat->last()->pluck('preco')->sum();
													
															if($first == 0) {
																foreach($produtosPorDataECat->sortByDesc('criado_em') as $produtos){
																	$first = $produtos->pluck('preco')->sum() != 0 ? $produtos->pluck('preco')->sum() : $first;
																}
															}
															if($last == 0) {
																foreach($produtosPorDataECat->sortBy('criado_em') as $produtos){
																	$last = $produtos->pluck('preco')->sum() != 0 ? $produtos->pluck('preco')->sum() : $last;
																}
															}
															$variacaoAcumulada = round((($last/$first)-1)*100, 2);

															if ($variacaoAcumulada < 0) {
																$class = 'text-danger';
															}elseif($variacaoAcumulada > 0){
																$class = 'text-success';	
															}else{
																$class = '';
															}
														@endphp
														<strong class="ml-auto {{ $class }}">{{ $class == 'text-success' ? '+' : '' }}{{ $variacaoAcumulada }}%</strong>
													</li>
												@endforeach
											</ul>
										</div>
									</div>
								</div>
								<div class="col-12 mt-3">
									<div class="card">
										<div class="card-header text-center">
											<strong class="card-title">Variação por categoria (soma dos preços):</strong>
										</div>
										<div class="card-body">
											<canvas id="lineVarCategorias" class="d-block w-100" height=""></canvas>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

    </div>
</div>
@endsection
