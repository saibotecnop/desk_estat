<div class="col-md-3">
	<div class="card">
        <div class="card-header">Menu</div>

		<div class="list-group list-group-flush sidebar">
			<a href="{{ route('home') }}" class="list-group-item list-group-item-action preloader {{ Route::is('home') ? 'active' : '' }}">
				Dashboard
			</a>
			<a href="{{ route('produtos') }}" class="list-group-item list-group-item-action preloader {{ Route::is('produtos*') ? 'active' : '' }}">
				Produtos
			</a>
			<a class="list-group-item list-group-item-action d-flex justify-content-center {{ Route::is('estatisticas*') ? 'active' : '' }}">
				Análise estatística
				<span class="badge-pill badge-info ml-auto">Em breve!</span>
			</a>
			<a href="{{ route('sobre') }}" class="list-group-item list-group-item-action {{ Route::is('sobre*') ? 'active' : '' }}">
				Sobre
			</a>
		</div>
   	</div>
</div>