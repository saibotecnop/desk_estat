@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
    	@include('partials.sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-header d-flex">
                	<span class="align-self-center">Editar <strong>{{ $produto->nome }}</strong></span>
                </div>
				
                <div class="card-body">
                	<div class="form-group">
	                	<form action="{{ route('produtos.update', ['produto'=>$produto->id]) }}" method="POST" enctype="multipart/form-data">
	                		@csrf
	                		<input type="hidden" name="url" value="{{ $produto->url }}">
	                		<div class="row">
	                			<div class="col-md-4">
	                				<div class="form-group col-12">
				                        <label for="categoria">Categoria</label>
				                       	<select class="form-control" name="categoria" id="">
				                       		<option disabled>Selecione...</option>
				                       		<option value="flores">Flores</option>
				                       		<option value="calcados">Calçados</option>
				                       		<option value="perfumes">Perfumes</option>
				                       		<option value="aparelhos_celulares">Aparelhos Celulares</option>
				                       		<option value="moda">Moda</option>
				                       		<option value="eletroportateis">Eletroportáteis</option>
				                       		<option value="alimento">Alimento</option>
				                       	</select>
				                    </div>
	                			</div>
	                			<div class="col-md-2 align-content-center justify-content-center">
	                				<div class="col-md-12 justify-content-center align-content-center">
								    	<img src="{{ (isset($produto) && $produto->full_url_foto) ? $produto->full_url_foto : 'https://fakeimg.pl/300/?text==)' }}" alt="" class="img-thumbnail">
								    </div>
								    <div class="col-md-12 pt-3">
								    	<input type="file" name="url_foto" class="custom-file-input upload-input" id="url_foto" style="display: none;">
								    	<label for="url_foto" class="btn btn-outline-secondary col-12 upload-button form-control {{ $errors->has('url_foto') ? 'is-invalid' : '' }}">{{ (isset($produto) && $produto->full_url_foto) ? 'Trocar foto' : 'Selecionar foto' }}</label>
								    	@if ($errors->has('url_foto'))
								            <span class="invalid-feedback" role="alert">
								                <strong>{{ $errors->first('url_foto') }}</strong>
								            </span>
								        @endif
								    </div>
							    </div>
	                		</div>
                			<button type="submit" class="btn btn-primary preloader">Atualizar</button>	
	                	</form>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
