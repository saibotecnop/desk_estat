@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
    	@include('partials.sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-header d-flex">
                	<span class="align-self-center">Produtos</span>
                </div>

                <div class="card-body p-0">
					<table class="table">
						<thead>
							<tr>
								<th colspan="2">No.</th>
								<th>Nome</th>
								<th>Categoria</th>
								<th>Ultima coleta</th>
								<th>Variação acumulada</th>
							</tr>
						</thead>
						<tbody>
							@forelse ($produtos->groupBy('url') as $produto)
								<tr>
									<td class="pr-0">{{ $loop->iteration }}</td>
									<td class="px-0" style="width: 80px">
										<div class="col-md-12 p-0 text-center">
											<img src="{{ $produto->first()->full_url_foto ? $produto->first()->full_url_foto : 'https://fakeimg.pl/300/?txt==)' }}" alt="" class="img-fluid" style="width: 50px">
										</div>
									</td>
									<td>
										{{ $produto->first()->nome }}
										<div class="row">
											<div class="col-12">
												<ul class="table-actions px-0">
													<li>
														<a href="{{ $produto->first()->url }}" target="_blank">Visitar site</a>
													</li>
													<li>
														<a href="{{ route('produtos.detalhes', ['produto'=>$produto->first()->id]) }}">Observações</a>
													</li>
													@auth
														<li>
															<a href="{{ route('produtos.edit', ['produto'=>$produto->first()->id]) }}">Editar</a>
														</li>
													@endauth
												</ul>
											</div>
										</div>
									</td>
									<td>{{ ucfirst($produto->first()->categoria) }}</td>
									<td class="text-center">
										@php
											if($produto->first()->preco != 0){
												$variacaoDia1 = round((($produto->last()->preco - $produto->first()->preco)/$produto->first()->preco)*100, 2);
												if ($variacaoDia1 < 0) {
													$classVariacao = 'text-danger';
												}elseif($variacaoDia1 > 0){
													$classVariacao = 'text-success';
												}else{
													$classVariacao = '';
												}
											}else{
												$classVariacao = '';
												$variacaoDia1 = '-';
											}
										
										@endphp
										<strong class="{{ $classVariacao }}">{{ $variacaoDia1 }}%</strong>
										
									</td>
									<td class="text-center">
										@php
																		
											if($produto->get($produto->count() - 2) && $produto->get($produto->count() - 2)->preco != 0){
												$variacaoDiaria = round((($produto->last()->preco - $produto->get($produto->count() - 2)->preco)/$produto->get($produto->count() - 2)->preco)*100, 2);
												if ($variacaoDiaria < 0) {
													$classVariacaoDia = 'text-danger';
												}elseif($variacaoDiaria > 0){
													$classVariacaoDia = 'text-success';
												}else{
													$classVariacaoDia = '';
												}
											}else{
												$classVariacaoDia = '';
												$variacaoDiaria = '-';
											}
										@endphp
										<strong class="{{ $classVariacaoDia }}">{{ $variacaoDiaria }}%</strong>
									</td>
								</tr>
							@empty
								<tr class="odd"><td valign="top" colspan="3" class="dataTables_empty">Nenhum registro encontrado</td></tr>	
							@endforelse
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
