@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
    	@include('partials.sidebar')
        <div class="col-md-9">
        	<input type="hidden" id="urlProduto" value="{{ $produtoBase->url }}">
        	<div class="card">
        		<div class="card-header">
        			<canvas id="singleProduct" class="d-block w-100" height="250"></canvas>
        		</div>
        	</div>
            <div class="card">
                <div class="card-header d-flex">
                	<span class="align-self-center">Observações para <strong>{{ $produtoBase->nome }}</strong></span>
                </div>

                <div class="card-body p-0">
					<table class="table">
						<thead>
							<tr>
								<th>Coleta</th>
								<th>Preço</th>
								<th>Variacao diaria</th>
								<th>Data da coleta</th>
							</tr>
						</thead>
						<tbody>
							@forelse ($produtos as $produto)
								@php
								if($loop->iteration != 1 && $produtos->get($loop->iteration - 2)->preco != 0 && $produto->preco != 0){
									$variacaoDiaria = $loop->iteration != 1 ? round((($produto->preco / $produtos->get($loop->iteration - 2)->preco)-1)*100, 2)."%" : "-";
									if($variacaoDiaria > 0){
										$classVariacao = 'text-success';
									}elseif($variacaoDiaria < 0){
										$classVariacao = 'text-danger';
									}else{
										$classVariacao = '';
									}
								}else{
									if ($produto->preco == 0) {
										$variacaoDiaria = 'Produto indisponível';
									
										$classVariacao = '';
									}else{
										$variacaoDiaria = 'N/A';
										$classVariacao = '';
									}
									
								}
								@endphp
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>R$ {{ $produto->preco }}</td>
									<td><strong class="{{ $classVariacao }}">{{ $variacaoDiaria }}</strong></td>
									<td>{{ $produto->criado_em->format('d-M') }}</td>
								</tr>
							@empty
								<tr class="odd"><td valign="top" colspan="3" class="dataTables_empty">Nenhum registro encontrado</td></tr>	
							@endforelse
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
