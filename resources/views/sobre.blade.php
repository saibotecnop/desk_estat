@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
    	@include('partials.sidebar')
    	<div class="col-md-6">
        	<div class="row">
        		@foreach ($textos as $texto)
    				<div class="col-md-12 mb-3">
	        			<div class="card">
			                <div class="card-header justify-content-center d-flex">
			                	<span class="m-0 p-0 d-flex">{{ $texto->titulo }}</span>
			                	@auth
			                		<a href="{{ route('sobre.edit', ['texto'=>$texto->id]) }}" class="btn btn-sm btn-outline-primary ml-auto">Editar</a>
			                	@endauth
			                </div>

			                <div class="card-body">
			                	@php
			                		echo $texto->corpo
			                	@endphp
							</div>
						</div>
					</div>
        		@endforeach
			</div>

		</div>
		<div class="col-md-3">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="card">
		                <div class="card-header font-weight-bold">Grupo</div>

		                <div class="card-body p-0">
	                		<ul class="list-group-flush p-0 m-0">
	                			<li class="list-group-item ">Gabriel Pereira</li>
	                			<li class="list-group-item ">Renato Cavichione</li>
	                			<li class="list-group-item ">Theo Girotto</li>
	                			<li class="list-group-item ">Tobias Ponce</li>
	                		</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
    </div>
</div>
@endsection
