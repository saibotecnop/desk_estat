@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
    	@include('partials.sidebar')
    	<div class="col-md-9">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="card">
		                <div class="card-header">Editando {{ $texto->titulo }}</div>

		                <div class="card-body">
		                	<div class="form-group">
			                	<form action="{{ route('sobre.update', ['texto'=>$texto->id]) }}" method="POST">
			                		@csrf
			                		<div class="row">
			                			<div class="col-md-12">
			                				<div class="form-group col-12">
						                        <label for="categoria">Titulo</label>
						                       	<input type="text" class="form-control" name="titulo" value="{{ $texto->titulo }}">
						                    </div>
			                			</div>
			                			<div class="col-md-12">
			                				<div class="form-group col-12">
						                        <label for="categoria">Corpo</label>
						                       	<textarea class="form-control" id="texto1" name="corpo" cols="30" rows="10">
						                       		{{ $texto->corpo }}
						                       	</textarea>
						                    </div>
			                			</div>

			                		</div>
			                		<div class="col-12">
			                			<button type="submit" class="btn btn-primary preloader">Atualizar</button>
			                		</div>
			                	</form>
			                </div>
						</div>
					</div>
				</div>
			</div>

		</div>

    </div>
</div>
@endsection
