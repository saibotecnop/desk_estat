<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	return redirect()->route('home');
});
Route::get('/home', function(){
	return redirect()->route('home');
});
Route::get('/dashboard', 'HomeController@index')->name('home');
Route::get('/produtos', 'ProdutoController@produtos')->name('produtos');
Route::get('/produtos/{produto}/detalhes', 'ProdutoController@detalhes')->name('produtos.detalhes');
Route::get('/estatisticas', 'ProdutoController@estatisticas')->name('estatisticas');

Route::post('/produtos/getData', 'ProdutoController@dataProduto')->name('produtos.data');
Route::post('/produtos/donutCategorias', 'ProdutoController@donutCategorias');
Route::post('/produtos/lineVarCategorias', 'ProdutoController@lineVarCategorias');

Route::get('/sobre', 'TextoController@index')->name('sobre');



Route::group(['middleware'=>'auth'], function(){
	Route::get('/produtos/{produto}/edit', 'ProdutoController@edit')->name('produtos.edit');
	Route::post('/produtos/{produto}/update', 'ProdutoController@update')->name('produtos.update');

	Route::get('/sobre/{texto}/edit', 'TextoController@edit')->name('sobre.edit');
	Route::post('/sobre/{texto}/update', 'TextoController@update')->name('sobre.update');
});

Auth::routes();


